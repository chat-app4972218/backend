//Setup our dependencies
const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");

// for the lient server dependencies
const app = express();
const server = require('http').createServer(app);
const io = require('socket.io')(server, { cors: {origin: "*"}});

const userRoutes = require("./routes/userRoutes");

// Server Setup
const port = 3001;


// MongoDB Connection
mongoose.connect("mongodb+srv://joram_182:joramape182@zuitt-bootcamp.kq3szvv.mongodb.net/chat-app?retryWrites=true&w=majority", 
	{
		useNewUrlParser : true,
		useUnifiedTopology : true
	}
);

mongoose.connection.once('open', () => console.log('Now connected to mongoDB Atlas!'));

// for Cross-origin Resource
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({extended:true}));

// Declaring main routes
app.use("/users", userRoutes);


server.listen(port, () => console.log(`API is now online port ${port}`));

io.on('connection', (socket) => { 
	// console.log("User connected: " + socket.id)}
	socket.on('receive-chat', (event) => {
		console.log('received chat');
		console.log(event);
		io.emit('broadcast', event)
	});
	socket.on('disconnect', () => {
		console.log('socket connection disconnected!', ' ')
	})
});