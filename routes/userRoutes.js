const express = require("express");
const router = express.Router();
const auth =  require("../auth");

const userController = require("../controller/userController");


// Register User
router.post("/register", (req, res) => {

	userController.registerUser(req.body).then(result => res.send(result));
})


router.get("/details", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization);
	console.log('=====> userData: ', userData);
	userController.getProfile(userData).then(resultFromController => {
		res.send(resultFromController)
	});
})


// route for user authentication
router.post("/login", (req, res) => {

	userController.loginUser(req.body).then(result => res.send(result));
})


// retreive client user
router.get("/client", (req, res) => {

	userController.getAllClient().then(result => res.send(result));

})


// Allows us to export the "router" object that will be accessed in our "index.js" file
module.exports = router;