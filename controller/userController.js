const User = require("../models/User");
const bcrypt = require("bcrypt");
const auth = require("../auth");


// Register User
module.exports.registerUser = (data) => {

	// The find the first record in the collection that matches the search criteria
	return User.findOne({email : data.email}).then(result => {

		if (result) { // if the email is exist

			return false;

		} else { // if the email didn't exist

		  let newUser = new User({

		  	firstName : data.firstName,
		  	lastName : data.lastName,
		  	email : data.email,
		  	password : bcrypt.hashSync(data.password, 10)

		  })

		  return newUser.save().then((user, error) => {

		  	if (error){

		  		return false;

		  	} else {

		  		return true;
		  	}
		})}
	})
}

// login user authenticatioon
module.exports.loginUser = (reqBody) => {

	// The find the first record in the collection that matches the search criteria
	return User.findOne({email : reqBody.email}).then(result => {
		//User doesn't exist
		if(result == null){
			return false;

		// User exist
		} else {

			// compare a non encrypted password from the login form to the excrypted password retrived from the database and it returns "true" of "false" value depending on the result
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password);
			if(isPasswordCorrect){

				// Generate an access token
				return {access : auth.createAccessToken(result)}
			} else {

				return false;
			}
		}
	})
}

// Get User Profile
module.exports.getProfile = (data) => {
	return User.findOne({email: data.email}).then(result => {
		if (result == null) {
			return false
		} else {
			result.password = ""
			return result
		}
	});
};

// get all client user 
	module.exports.getAllClient = () => {

		return User.find({isAdmin : false}).then(result => {
			// result.password = " ";
			return result;
		})
	}